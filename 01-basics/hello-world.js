"use strict";
/*
console.log("Hello, world!")

let myName: string;
myName = "Alice";
myName = "Bob";
// myName = 42;

let myAge: number;
myAge = 42;
myAge = 18;
// myAge = false;

let needsCoffe: boolean;
needsCoffe = true;
needsCoffe = false;
// needsCoffe = "42";
*/
Object.defineProperty(exports, "__esModule", { value: true });
/*
let myName: string = "Alice";
let myAge: number = 42;
let needsCoffe: boolean = true;
*/
var myName = "Alice"; // Typen blir automatiskt string
var myAge = 42; // Typen blir automatiskt number
var needsCoffe = true; // Typen blir automatiskt boolean
