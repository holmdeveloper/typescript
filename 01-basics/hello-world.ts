/*
console.log("Hello, world!")

let myName: string;
myName = "Alice";
myName = "Bob";
// myName = 42;

let myAge: number;
myAge = 42;
myAge = 18;
// myAge = false;

let needsCoffe: boolean;
needsCoffe = true;
needsCoffe = false;
// needsCoffe = "42";
*/

/*
let myName: string = "Alice";
let myAge: number = 42;
let needsCoffe: boolean = true;
*/

let myName = "Alice";  // Typen blir automatiskt string
let myAge = 42;        // Typen blir automatiskt number
let needsCoffe = true; // Typen blir automatiskt boolean

// needsCoffe = null;     // Type 'null' is not assignable to type 'boolean'.ts(2322)

export {};
