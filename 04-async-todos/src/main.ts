import axios from 'axios';
import "./assets/scss/app.scss"

const todosEl = document.querySelector<HTMLUListElement>("#todos")!;
const newTodoFormEl = document.querySelector<HTMLFormElement>("#new-todo-form")!;
const todoDescriptionPopup = document.querySelector("#task-popup") as HTMLElement;

type Todo = {
  id: number;
  title: string;
  completed: boolean;
  important:boolean;
  priority: 'low' | 'medium' | 'high'; // Lägg till prioriteringsnivå
  tags: string[]; 
  description: string;
};

let todos: Todo[] = [];

const fetchTodos = async () => {
  try {
    const response = await axios.get('http://localhost:3000/todos');
    todos = response.data;
    renderTodos(); // Uppdatera gränssnittet direkt
  } catch (error) {
    console.error('Error fetching todos:', error);
  }
};

// Lägg till en klicklyssnare på dina uppgiftselement i listan
todosEl.addEventListener('click', (e) => {
  const targetElement = e.target as HTMLElement;
  if (targetElement && targetElement.classList.contains('list-group-item')) {
    const todoId = Number(targetElement.getAttribute('data-id'));
    const selectedTodo = todos.find((todo) => todo.id === todoId);

    // Visa beskrivningen för den valda uppgiften som en popup
    if (selectedTodo) {
      displayTodoDescriptionPopup(selectedTodo);
    }
  }
});

// Funktion för att visa beskrivningen som en popup
  function displayTodoDescriptionPopup(todo: Todo) {
    // Uppdatera popupens innehåll med den valda uppgiftens beskrivning
    const todoDescriptionPopupTitle = document.querySelector("#popup-title") as HTMLElement;
    const todoDescriptionPopupDescription = document.querySelector("#popup-description") as HTMLElement;

    todoDescriptionPopupTitle.textContent = todo.title;
    todoDescriptionPopupDescription.textContent = todo.description;

    // Visa popupen genom att ta bort 'hidden' klassen
    todoDescriptionPopup.classList.remove('hidden');
  }

  // Lägg till klicklyssnare för att stänga popupen
  const closeDescriptionPopupButton = document.querySelector("#close-popup-button") as HTMLElement;
  if (closeDescriptionPopupButton) {
    closeDescriptionPopupButton.addEventListener('click', () => {
      hideTodoDescriptionPopup();
    });
  }

  // Funktion för att dölja popupen
  function hideTodoDescriptionPopup() {
    todoDescriptionPopup.classList.add('hidden');
  }


const createTodo = async (newTodo: Todo) => {
  try {
    const response = await axios.post('http://localhost:3000/todos', newTodo);
    const createdTodo = response.data;
    todos.push(createdTodo); // Lägg till den nya todo i arrayen

    renderTodos(); // Uppdatera gränssnittet direkt
  } catch (error) {
    console.error('Error creating todo:', error);
  }
};

const deleteTodo = async (todoId: number) => {
  try {
    await axios.delete(`http://localhost:3000/todos/${todoId}`);
    todos = todos.filter((todo) => todo.id !== todoId);
    renderTodos(); // Uppdatera gränssnittet direkt
    } catch (error) {
    console.error('Error deleting todo:', error);
  }
};

const updateTodo = async (todoId: number, completed: boolean) => {
  try {
    const todo = todos.find((t) => t.id === todoId);
    if (todo) {
      todo.completed = completed; // Uppdatera completed-statusen
      const response = await axios.put(`http://localhost:3000/todos/${todoId}`, todo); // Skicka hela todo-objektet
      const updatedTodo = response.data;
      const index = todos.findIndex((t) => t.id === updatedTodo.id);
      if (index !== -1) {
        todos[index] = updatedTodo;
        renderTodos(); // Uppdatera gränssnittet direkt
      }
  
    }
  } catch (error) {
    console.error('Error updating todo:', error);
  }
};
todosEl.addEventListener('click', (e) => {
  const targetElement = e.target as HTMLElement;
  if (targetElement && targetElement.classList.contains('clickable-title')) {
    const todoId = Number(targetElement.closest('.list-group-item')?.getAttribute('data-id'));
    const selectedTodo = todos.find((todo) => todo.id === todoId);

    // Visa beskrivningen för den valda uppgiften som en popup
    if (selectedTodo) {
      displayTodoDescriptionPopup(selectedTodo);
    }
  }
});

// Funktion för att visa detaljerna för en enskild tod
const renderTodos = () => {
  if (todos.length === 0) {
    todosEl.innerHTML = '<p class="no-task-message">No task :(</p>';
  } else {
    todosEl.innerHTML = todos
      .map((todo) => {
        let priorityIcon;
        switch (todo.priority) {
          case 'low':
            priorityIcon = '<i class="fas fa-arrow-down priority low"></i>';
            break;
          case 'medium':
            priorityIcon = '<i class="fas fa-minus priority medium"></i>';
            break;
          case 'high':
            priorityIcon = '<i class="fas fa-arrow-up priority high"></i>';
            break;
          default:
            priorityIcon = '';
            break;
        }
        const tagsHTML = todo.tags.map((tag) => `<span class="tag">${tag}</span>`).join('');
        return `<li class="list-group-item ${todo.completed ? 'completed' : ''}" data-id="${todo.id}">
          <div class="todo-item-container">
            <span class="circle ${todo.important ? 'important' : ''}"></span>
            <span class="todo-title">
              <span class="clickable-title">${todo.title}</span> <!-- Klickbar titel -->
            </span>
            <span class="priority">${priorityIcon}</span>
            <span class="todo-item">${tagsHTML}</span>
          </div>
    
          <div class="button-container">
            <button class="btn btn-danger delete-button" data-id="${todo.id}">Delete</button>
            <button class="btn ${todo.completed ? 'btn-secondary' : 'btn-primary'} complete-button" data-id="${todo.id}">
              ${todo.completed ? 'Uncomplete' : 'Complete'}
            </button>
          </div>
       
        </li>`;
       
      })
      .join('');
  }
};

todosEl.addEventListener('click', (e) => {
  const targetElement = e.target as HTMLElement;
  if (targetElement && targetElement.classList.contains('delete-button')) {
    const todoId = Number(targetElement.getAttribute('data-id'));
    deleteTodo(todoId);
  }
});

todosEl.addEventListener('click', (e) => {
  const targetElement = e.target as HTMLElement;
  if (targetElement && targetElement.classList.contains('complete-button')) {
    const todoId = Number(targetElement.getAttribute('data-id'));
    const todo = todos.find((t) => t.id === todoId);
    if (todo) {
      // Invertera "completed" statusen och uppdatera todo
      const completed = !todo.completed;
      updateTodo(todoId, completed);
    }
  }
});

newTodoFormEl.addEventListener('submit', (e) => {
  e.preventDefault();
  const newTodoTitleEl = document.querySelector<HTMLInputElement>("#new-todo-title")!;
  const newTodoImportantEl = document.querySelector<HTMLInputElement>("#new-todo-important")!;
  const newTodoPriorityEl = document.querySelector<HTMLSelectElement>("#new-todo-priority")!;
  const newTodoTagsEl = document.querySelector<HTMLInputElement>("#new-todo-tags")!;
const selectedPriority = newTodoPriorityEl.value;
const newTodoDescriptionEl = document.querySelector<HTMLInputElement>("#new-todo-description")!; 

// Kontrollera om den valda prioriteringsnivån är giltig
if (selectedPriority === "low" || selectedPriority === "medium" || selectedPriority === "high") {
  const newTodo: Todo = {
    id: Date.now(),
    title: newTodoTitleEl.value,
    completed: false,
    important: newTodoImportantEl.checked,
    priority: selectedPriority,
    tags: newTodoTagsEl.value.split(',').map((tag) => tag.trim()), 
    description: newTodoDescriptionEl.value, // Hämta beskrivningen från inputfältet
  };

  createTodo(newTodo);
  newTodoTitleEl.value = '';
  newTodoImportantEl.checked = false; // Återställ kryssrutan efter att ha skapat todo
  newTodoDescriptionEl.value = ''; // Återställ beskrivningsfältet efter att ha skapat todo
} else {
  console.error("Invalid priority value.");
  // Hantera felaktiga prioriteringsvärden på lämpligt sätt här
}
});

renderTodos();
fetchTodos();

function displayTodoDetails(selectedTodo: Todo) {
  throw new Error('Function not implemented.');
}
