import "./assets/scss/app.scss"

// Definiera en typ för todos
type Todo = {
  title: string;
  completed: boolean;
  important: boolean;
};

// Definiera konstanter för DOM-element med TypeScript-typer
const todoList: HTMLUListElement | null = document.querySelector('#todos');
const newTodoForm: HTMLFormElement | null = document.querySelector('#new-todo-form');
const newTodoTitleInput: HTMLInputElement | null = document.querySelector('#new-todo-title');
const newTodoImportantCheckbox: HTMLInputElement | null = document.querySelector('#new-todo-important');

// Funktion för att lagra todos i lokal lagring
function saveTodosToLocalStorage(todos: Todo[]): void {
  localStorage.setItem('todos', JSON.stringify(todos));
}

// Funktion för att hämta todos från lokal lagring
function getTodosFromLocalStorage(): Todo[] {
  const todosJSON = localStorage.getItem('todos');
  return todosJSON ? JSON.parse(todosJSON) : [];
}

// Lista för att lagra todos
let todos: Todo[] = getTodosFromLocalStorage();

// Funktion för att visa todos
function renderTodos(): void {
  if (todoList === null) {
    return;
  }

  todoList.innerHTML = '';

  todos.forEach((todo, index) => {
    const listItem = document.createElement('li');
    listItem.classList.add('list-group-item');
    if (todo.completed) {
      listItem.classList.add('completed');
    }
    if (todo.important) {
      listItem.classList.add('important');
    }
    listItem.innerHTML = `
      ${todo.title}
      <button class="btn btn-danger btn-sm float-end" data-index="${index}">X</button>
      <button class="btn btn-success btn-sm float-end" data-index="${index}">Complete</button>
    `;

    // Lägg till händelselyssnare för att ta bort todo när "Delete" -knappen klickas
    const deleteButton = listItem.querySelector('.btn-danger');
    if (deleteButton !== null) {
      deleteButton.addEventListener('click', () => {
        todos.splice(index, 1);
        saveTodosToLocalStorage(todos); // Uppdatera lokal lagring
        renderTodos();
      });
    }

    // Lägg till händelselyssnare för att markera todo som klar när "Complete" -knappen klickas
    const completeButton = listItem.querySelector('.btn-success');
    if (completeButton !== null) {
      completeButton.addEventListener('click', () => {
        todos[index].completed = true;
        saveTodosToLocalStorage(todos); // Uppdatera lokal lagring
        renderTodos();
      });
    }

    todoList.appendChild(listItem);
  });
}

// Lägg till händelselyssnare för formuläret för att skapa nya todos
if (newTodoForm !== null) {
  newTodoForm.addEventListener('submit', (e) => {
    e.preventDefault();
    if (newTodoTitleInput !== null) {
      const newTodoTitle = newTodoTitleInput.value.trim();
      if (newTodoTitle) {
        const newTodoImportant = newTodoImportantCheckbox !== null && newTodoImportantCheckbox.checked;
        const newTodo: Todo = { title: newTodoTitle, completed: false, important: newTodoImportant };
        todos.push(newTodo);
        saveTodosToLocalStorage(todos); // Uppdatera lokal lagring
        newTodoTitleInput.value = '';
        if (newTodoImportantCheckbox !== null) {
          newTodoImportantCheckbox.checked = false;
        }
        renderTodos();
      }
    }
  });
}

// Kör renderTodos() för att visa todos vid start
renderTodos();